<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Reversemtn extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'reversemtn';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'AByster Consulting.';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Reverse payment MTN Mobile');
        $this->description = $this->l('Use to reverse payment on MTN mobile money');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        //Configuration::updateValue('RV_MTN_CONSUMER_URL','https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml');

        return parent::install()
        && Configuration::updateValue('RV_MTN_CONSUMER_URL','https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml')
        &&  $this->registerHook('header')
        &&  $this->registerHook('backOfficeHeader')
        &&  $this->registerHook('actionSendPaymentMTN')
        ;
    }

    public function uninstall()
    {
        Configuration::deleteByName('JMARKETPLACE_MOBILEMONEY_PHONE_NUMER');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitReverseMTNModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        return $this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitReverseMTNModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings MTN mobile'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(

                    array(
                        'type' => 'text',
                        'label' => $this->l('Sender MTN phone number'),
                        'prefix' => '<i class="icon icon-phone"></i>',
                        'name' => 'JMARKETPLACE_MOBILEMONEY_PHONE_NUMER',
                        'desc' => $this->l('This phone number is used to send MTN mobile payment.'),
                        'required' => true,
                        'lang' => false,
                        'col' => 3,
                    ),
                ),

                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );

    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(

            'JMARKETPLACE_MOBILEMONEY_PHONE_NUMER' => Configuration::get('JMARKETPLACE_MOBILEMONEY_PHONE_NUMER')
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    /**
    * Action to validate MTN mobile money
    * params = order_amount, receiver_mail
    * returns :
    *           code != 0 ou == 1 : success
    *           code = '' ou == 0 : error
    */

    public function hookActionSendPaymentMTN($params) {
        //Hook::exec('actionValidatePaymentMTN', $params_hook);

        $order_amount = $params['order_amount'];
        $receiver_mail = $params['receiver_mail'];
        $sender_phone = Configuration::get('JMARKETPLACE_MOBILEMONEY_PHONE_NUMER');

        // Check that this payment option is still available in case the customer changed his address just before the end of the checkout process

        /*$authorized = false;

        foreach (Module::getPaymentModules() as $module)

        if ($module['name'] == 'mtnmobilemoneycmr')
        {
            $authorized = true;
            break;
        }

        if (!$authorized)
            die($this->module->l('This payment method is not available.', 'validation'));*/

        //check mtn mobile money number

        if (!preg_match("(^6[7]\d{7}$|^6[5][01234]\d{6}$|^6[8]\d{7}$)", $sender_phone))
        {
            $this->ablog('Sender Phone number customer invalid.','INFO');

            $ret = array(
                'code' => 0,
                'desc' => 'Phone number customer invalid.'
            );
            return $ret;
        }

        /*** prepare mtnc api parameter's call ***/
        //get consumer credentials

        $consumerUrl = trim(Configuration::get('RV_MTN_CONSUMER_URL'));//'https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml';
        $consumerEmail = trim($receiver_mail);

        $header=array();

        $header[]="Accept:application/json";


        //information sur la commission

        $amount = $order_amount+0; //remove zeros decimal

        $amount = ceil($amount); //round to next int value

        $uri = '?idbouton=2&typebouton=PAIE&_amount='.$amount;

        $uri .= '&_tel='. $sender_phone;

        $uri .= "&_clP=&_email=".$consumerEmail;

        $uri .= "&submit.x=104&submit.y=70";


        $url = $consumerUrl.$uri;

        $this->ablog('Validation du paiment ==> Initialisation du processus de paiement','INFO');

        $this->ablog('API Url '.$url,'INFO');


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL , $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $result = curl_exec($ch);
        curl_close($ch);

        $this->ablog('data in function '.$result,'INFO');

        $response = json_decode($result, true);

        $status_code = 0;

        if($response != null && !empty($response))$status_code = $response["StatusCode"]+0;
            $this->ablog('data in function '.$response,'INFO');

        if($status_code !== 0 && $status_code == 1) {
            // tout s'est bien passé
            $this->ablog('retour du web service pour la commande: '.$infos_commission[0]['reference'].' ==> '.$response["StatusDesc"],'INFO');

            /*$seller_commission_history = new SellerCommissionHistory($id_seller_commission_history);
            $seller_commission_history->id_seller_commission_history_state = SellerCommissionHistoryState::getIdByReference('paid');
            $seller_commission_history->update();

            $this->confirmations[] = $this->l('Votre paiement a été bien envoyé.');*/
            $ret = array(
                'code' => $status_code,
                'desc' => $response['StatusDesc']
            );

            return $ret;

        }else{

            //erreur de paiement retournée par l'API
            if($response["StatusDesc"] == '')
                $response["StatusDesc"] = 'Failed to load please verify your account';

            $this->ablog('erreur fonctionnelle : '.$infos_commission[0]['reference'].' ==> '.$response["StatusDesc"],'INFO');

            $ret = array(
                'code' => $status_code,
                'desc' => $response['StatusDesc']
            );

            return $ret;

        }
    }

    public function ablog($data,$level){

        $day = gmdate("Y-m-d");

        $logfile=dirname(__FILE__).'/log/reverseMTN_'.$day.'.log';

        error_log("\r\n".'['.gmdate("Y-m-d H:i:s").'] '.$level.' '.$data, 3, $logfile);

    }
}
